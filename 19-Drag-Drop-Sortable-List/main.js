const draggableList = document.getElementById('draggable-list'),
  checkBtn = document.getElementById('check');

const richestPeople = [
  'Jeff Bezos',
  'Mark Zukenberg',
  'Bill Gates',
  'Bernard Arnault',
  'Carlos Slim Helu',
  'Amancio Otega',
  'Lary Page',
  'Michael Bloomberg',
  'Warren Buffet',
  'Larry Ellison'
];
// store list items

const listItems = [];

let dragStartIndex;

createList();

// create list items 

function createList() {
  [...richestPeople]
    .map(a => ({ value: a, sort: Math.random() }))
    .sort((a, b) => a.sort - b.sort)
    .map(a => a.value)
    .forEach((person, index) => {
      const item = document.createElement('li');
      // item.classList.add('right');
      item.setAttribute('data-index', index);
      item.innerHTML = `
      <span class="number">${index + 1}</span>
      <div class="draggable" draggable="true"> 
        <p class="person-name">
          ${person}
        </p>
        <i class="fas fa-bars"></i>
      </div>
    `;
      listItems.push(item);
      draggableList.append(item);
    });

  addEventListeners();
}


function addEventListeners() {
  const draggables = document.querySelectorAll('.draggable');
  const dragistItems = document.querySelectorAll('.draggable-list li');

  draggables.forEach(draggable => {
    draggable.addEventListener('dragstart', dragStart)
  });
  dragistItems.forEach(item => {
    item.addEventListener('dragover', dragOver);
    item.addEventListener('drop', dragDrop);
    item.addEventListener('dragenter', dragEnter);
    item.addEventListener('dragleave', dragLeave);
  });

}

// functions

function dragStart() {
  dragStartIndex = +this.closest('li').getAttribute('data-index');
  console.log(dragStartIndex)
}

function dragOver(e) {
  e.preventDefault();
}

function dragDrop() {
  const dragEndIndex = +this.getAttribute('data-index');
  swapItem(dragStartIndex, dragEndIndex);
  this.classList.remove('over');
}

function dragEnter() {
  this.classList.add('over');
}
function dragLeave() {
  this.classList.remove('over');
}

// swap item in the list

function swapItem(fromIndex, toIndex) {
  const itemOne = listItems[fromIndex].querySelector('.draggable');
  const itemTwo = listItems[toIndex].querySelector('.draggable');
  listItems[fromIndex].appendChild(itemTwo);
  listItems[toIndex].appendChild(itemOne);
}

function checkOrder() {
  listItems.forEach((item, index) => {
    const personName = item.querySelector('.draggable').innerText.trim();
    // check if person name 
    if (personName !== richestPeople[index])
    {
      item.classList.add('wrong');
      item.classList.remove('right');
    } else
    {
      item.classList.remove('wrong');
      item.classList.add('right');
    }
  });
}

// check event

checkBtn.addEventListener('click', checkOrder);