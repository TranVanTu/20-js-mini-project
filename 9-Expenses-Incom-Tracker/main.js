const balance = document.getElementById('balance'),
  money_plus = document.getElementById('money-plus'),
  money_minus = document.getElementById('money-minus'),
  list = document.getElementById('list'),
  form = document.getElementById('form'),
  text = document.getElementById('text'),
  amount = document.getElementById('amount');

const dummyTransations = [
  { id: 1, text: 'Flower', amount: -20 },
  { id: 2, text: 'Done Project', amount: 200 },
  { id: 3, text: 'Buy a cat', amount: -100 },
  { id: 4, text: 'Wash the car', amount: 10 },
];

const localStorageTransactions = JSON.parse(localStorage.getItem('transactions'));
let transactions = localStorage.getItem('transactions') !== null ? localStorageTransactions : [];
// add transactions to DOM list

function addTransactionDOM(transaction) {
  //  get sign

  const sign = transaction.amount < 0 ? '-' : '+';
  const item = document.createElement('li');

  // add class to li item based on value

  item.classList.add(transaction.amount < 0 ? 'minus' : 'plus');
  item.innerHTML = `
    ${transaction.text}
    <span>
      ${sign}${Math.abs(transaction.amount)}
    </span>
    <button onclick="removeTransaction(${transaction.id})" class="delete-btn">x</button>
  `;
  list.appendChild(item);

}

// update the balance income and expense
function updateValues() {
  const amounts = transactions.map(transaction => {
    return transaction.amount;
  });
  const total = amounts.reduce((accum, item) => {
    return accum += item;
  }, 0).toFixed(2);
  const income = amounts.filter(amount => amount > 0).reduce((acc, item) => {
    return acc += item;
  }, 0).toFixed(2);
  const expense = amounts.filter(amount => amount < 0).reduce((acc, item) => {
    return acc += item;
  }, 0).toFixed(2);
  console.log('income is : ' + income);
  console.log('expense is : ' + expense);
  balance.innerText = `${total}`;
  money_plus.innerText = `$${income}`;
  money_minus.innerText = `$${expense}`;
}

updateValues();
// init app

function init() {
  list.innerHTML = '';
  transactions.forEach(transaction => {
    addTransactionDOM(transaction);
  });
}
init();

// update localStorageTransactions

function upDateLocalStorage() {
  localStorage.setItem('transactions', JSON.stringify(transactions));
}
// form event listener
form.addEventListener('submit', addTransaction);

function addTransaction(e) {
  // prevent default behavior
  e.preventDefault();

  // 
  if (text.value.trim() === '' || amount.value.trim() === '')
  {
    alert('Please add a text and amount');
  } else
  {
    const transaction = {
      id: generateID(),
      text: text.value,
      amount: +amount.value
    };
    transactions.push(transaction);
    addTransactionDOM(transaction);
    updateValues();
    upDateLocalStorage();
    // clear the input
    text.value = '';
    amount.value = '';
    console.log(transaction);
  }

}

// generate random ID
function generateID() {
  return Math.round(Math.random() * 10000000);
}

// remove transaction from the DOM
function removeTransaction(id) {
  transactions = transactions.filter(transaction => transaction.id !== id);
  upDateLocalStorage();
  init();
}