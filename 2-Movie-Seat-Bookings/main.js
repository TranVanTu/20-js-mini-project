var container = document.querySelector('.container');
var seats = document.querySelectorAll('.row .seat:not(.occupied)');
var count = document.querySelector('.count');
var total = document.querySelector('.money');
var movieList = document.getElementById('pick-movie');
var ticketPrice = parseInt(movieList.value);
console.log(ticketPrice)
container.addEventListener('click', (e) => {
  if (e.target.classList.contains('seat') === true && e.target.classList.contains('occupied') === false)
  {
    console.log('ok');
    e.target.classList.toggle('selected');
    updateSelectedCount();

  }
});

// update count
function updateSelectedCount() {

  const selectedSeats = document.querySelectorAll('.row .seat.selected');

  // copy indexes selected seats into array
  var seatIndexes = [...selectedSeats].map(seat =>
    [...seats].indexOf(seat)
  );
  // console.log(seatIndexes);

  localStorage.setItem('selectedSeats', JSON.stringify(seatIndexes));
  populateUI()
  // map through array

  // return a new array of indexes

  // console.log(selectedSeats);
  const selectedSeatsCount = selectedSeats.length;
  // console.log(selectedSeatsCount);
  count.textContent = selectedSeatsCount;
  total.textContent = ticketPrice * selectedSeatsCount;
}
// get data from local storage and populate UI
function populateUI() {
  const selectedSeats = JSON.parse(localStorage.getItem('selectedSeats'))
  if (selectedSeats !== null && selectedSeats.length > 0)
  {
    seats.forEach((seat, index) => {
      if (selectedSeats.indexOf(index) > -1)
      {
        seat.classList.add('selected')
      }
    });
  }
  const selectedMovieIndex = localStorage.getItem('selectedMovieIndex')
  const selectedMoviePrice = localStorage.getItem('selectedMoviePrice')
  if (selectedMovieIndex !== null)
  {
    movieList.selectedIndex = selectedMovieIndex;

  }
  if (selectedMoviePrice !== null)
  {
    ticketPrice = parseInt(selectedMoviePrice);
  }
}
// save selected movie index and price
function setMovieData(index, price) {
  localStorage.setItem('selectedMovieIndex', index)
  localStorage.setItem('selectedMoviePrice', price)
}
movieList.addEventListener('change', (e) => {
  ticketPrice = parseInt(movieList.value);
  setMovieData(e.target.selectedIndex, e.target.value);
  populateUI()
  updateSelectedCount();
});
populateUI();

// initial count and total
updateSelectedCount()