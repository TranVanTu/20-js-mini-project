const musicContainer = document.getElementById('music-container'),
  playBtn = document.getElementById('play'),
  prevBtn = document.getElementById('prev'),
  nextBtn = document.getElementById('next'),
  audio = document.getElementById('audio'),
  progress = document.getElementById('progress'),
  progressContainer = document.getElementById('progress-container'),
  cover = document.getElementById('cover'),
  title = document.getElementById('title');

// songs title
const songs = ['Dung-Lo-Anh-Doi-Ma-Mr-Siro', 'So-Phai-Ket-Thuc-Nhat-Phong', 'Thich-Thi-Den-Le-Bao-Binh'];

// Keep track of song
let songIndex = 0;

// initially load the song into DOM

function loadSong(song) {
  title.innerText = song;
  audio.src = `./imgs/${song}.mp3`;
  cover.src = `./imgs/${song}.jpg`;
  musicContainer.classList.add('play');
  playSong();
}

// event listeners
audio.addEventListener('timeupdate', updateProgress);
playBtn.addEventListener('click', function (e) {
  const isPlayed = musicContainer.classList.contains('play');

  if (isPlayed)
  {
    pauseSong();
  } else
  {
    playSong();
  }
});

// play song function
function playSong() {
  musicContainer.classList.add('play');
  playBtn.querySelector('i').classList.add('fa-pause');
  playBtn.querySelector('i').classList.remove('fa-play');
  audio.play();

}

// pause song
function pauseSong() {
  musicContainer.classList.remove('play');
  playBtn.querySelector('i').classList.remove('fa-pause');
  playBtn.querySelector('i').classList.add('fa-play');
  audio.pause();
}

//  change song
prevBtn.addEventListener('click', prevSong);
nextBtn.addEventListener('click', nextSong);

// previous song
function prevSong() {
  songIndex--;
  if (songIndex < 0)
  {
    songIndex = songs.length - 1;
  }
  loadSong(songs[songIndex]);
  playSong();
}

progressContainer.addEventListener('click', setProgress);

// set progress bar and time stamp

function setProgress(e) {
  const width = this.clientWidth;
  const clickX = e.offsetX;
  const duration = audio.duration;
  audio.currentTime = (clickX / width) * duration;
  console.log(duration);
  console.log(clickX);
}
// previous song
function nextSong() {
  songIndex++;
  if (songIndex > songs.length - 1)
  {
    songIndex = 0;
  }
  loadSong(songs[songIndex]);
  playSong();
}

// update progress bar

function updateProgress(e) {
  const { duration, currentTime } = e.srcElement;
  const progressPercent = (currentTime / duration) * 100;
  progress.style.width = `${progressPercent}%`;
}

// song end

audio.addEventListener('ended', nextSong);