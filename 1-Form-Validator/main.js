const form = document.getElementById('form');
const userName = document.getElementById('username');
const password = document.getElementById('password');
const confirmPassword = document.getElementById('confirm-password');
const email = document.getElementById('email');
const successRegister = document.querySelector('.success-register');
var check = true;
form.addEventListener('submit', (e) => {
  e.preventDefault();
  var msgUserName = document.querySelector('#username ~ .msg');
  var msgEmail = document.querySelector('#email ~ .msg');
  var msgPassword = document.querySelector('#password ~ .msg');
  var msgConfirmPassword = document.querySelector('#confirm-password ~ .msg');
  if (userName.value.length < 5)
  {
    msgUserName.innerHTML = 'User name must have at least 5 character';
    showError(userName);
    check = false;
  } else
  {
    msgUserName.innerHTML = 'Correct user name';
    showSuccess(userName);
    check = true;
  }
  if (email.value.includes('@') && email.value.length > 8)
  {
    msgEmail.innerHTML = "Correct Email Address";
    showSuccess(email);
    check = true;
  } else
  {
    msgEmail.innerHTML = "Emaill address must be at least 8 character and contains @ symbol";
    showError(email);
    check = false;
  }
  if (password.value.length > 6)
  {
    msgPassword.innerHTML = "Correct Password";
    showSuccess(password);
    check = true;
  } else
  {
    msgPassword.innerHTML = "Password must be at least 6 character";
    showError(password);
    check = false;
  }
  if (password.value === confirmPassword.value && password.value !== '')
  {
    msgConfirmPassword.innerHTML = "Password matched";
    showSuccess(confirmPassword);
    check = true;
  } else
  {
    msgConfirmPassword.innerHTML = "Password don't matched";
    showError(confirmPassword);
    check = false;
  }
  if (check)
  {
    form.style.transform = "scale(0)";
    successRegister.style.transform = "translateY(0)";
  }
});
function showError(input) {
  input.classList.add('error');
  input.classList.remove('success');
  input.style.borderColor = 'rgb(199, 19, 19)';
}
function showSuccess(input) {
  input.classList.remove('error');
  input.classList.add('success');
  input.style.borderColor = 'rgba(0, 128, 0, 0.801)';
}
console.log("submit");