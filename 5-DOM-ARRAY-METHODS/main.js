const main = document.getElementById('main');
const addBtn = document.getElementById('add-user');
const doubleBtn = document.getElementById('double-money');
const showMillionaresBtn = document.getElementById('show-millionares');
const sortRichestBtn = document.getElementById('sort-richest');
const calcEntireBtn = document.getElementById('calc-entire');
var calcTag = false;
var data = [];

// fetch random user and add money
async function getRandomUser() {
  const res = await fetch('https://randomuser.me/api');
  const data = await res.json();
  // console.log(data);
  const user = data.results[0];
  const newUser = {
    name: `
      ${user.name.first} ${user.name.last}
    `,
    money: Math.floor(Math.random() * 1000000)
  }
  addData(newUser);
}
getRandomUser();
getRandomUser();
getRandomUser();

// add new object to data array
function addData(obj) {
  data.push(obj);
  // console.log(obj);
  updateDOM();
}
// getRandomUser();

// update DOM
function updateDOM(provideData = data) {
  // clear main div
  main.innerHTML = '<h2> <strong>Person</strong><strong>Wealth</strong></h2>';
  provideData.forEach(data => {
    const element = document.createElement('div');
    element.classList.add('person');
    element.innerHTML = `<strong>${data.name}</strong><strong>${formatMoney(data.money)}</strong>`;
    main.appendChild(element);
  });
}
// updateDOM(data);
// console.log(data);

// format number as money

function formatMoney(number) {
  return '$' + number.toFixed(2).replace(/(\d)(?=(\d{3})+\b)/g, '$1 ');
}
// event listeners
addBtn.addEventListener('click', getRandomUser);
doubleBtn.addEventListener('click', doubleMoney);
sortRichestBtn.addEventListener('click', sortByRichest);
showMillionaresBtn.addEventListener('click', showMillionares);
calcEntireBtn.addEventListener('click', function () {
  if (!calcTag)
  {
    calculateWealth();
    calcTag = true;
  } else
  {
    calcTag = true;
  }
});
// double money function 
function doubleMoney() {
  data = data.map(user => {
    return { ...user, money: user.money * 2 };
  });
  updateDOM();
}

// sort by richest function
function sortByRichest() {
  data.sort((a, b) =>
    b.money - a.money
  );
  updateDOM();
}

// show millionares function
function showMillionares() {
  data = data.filter((user) => {
    return user.money > 1000000;
  });
  updateDOM();
}
// calculate total wealth

function calculateWealth() {
  const wealth = data.reduce((total, user) => (total += user.money), 0);
  const wealthElement = document.createElement('div');
  wealthElement.innerHTML = `
    <h3> <strong>Total : </strong><strong> ${formatMoney(wealth)}</strong></h3>
  `
  main.appendChild(wealthElement);
}