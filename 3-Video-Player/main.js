const video = document.getElementById('video');
const playBtn = document.getElementById('play');
const progress = document.getElementById('progress');
const stopBtn = document.getElementById('stop');
const timeStamp = document.getElementById('timestamp');
// video event listeners
video.addEventListener('click', toggleVideoStatus);
video.addEventListener('play', updatePlayIcon);
video.addEventListener('pause', updatePlayIcon);
video.addEventListener('timeupdate', updateProgress);

playBtn.addEventListener('click', toggleVideoStatus);
stopBtn.addEventListener('click', stopVideo);
progress.addEventListener('change', setVideoProgress);

// play and pause video
function toggleVideoStatus() {
  if (video.paused)
  {
    video.play();
  } else
  {
    video.pause();
  }
}
// update play & pause icon
function updatePlayIcon() {
  if (video.paused)
  {
    playBtn.innerHTML = `<i class="fas fa-play fa-2x"> </i>`;
  } else
  {
    playBtn.innerHTML = `<i class="fas fa-pause fa-2x"> </i> `;
  }
}
// update progress & timestamp
function updateProgress() {
  progress.value = (video.currentTime / video.duration) * 100;

  // get minutes
  var minutes = Math.floor(video.currentTime / 60);
  if (minutes < 10)
  {
    minutes = '0' + String(minutes);
  }
  // get seconds
  var seconds = Math.floor(video.currentTime % 60);
  if (seconds < 10)
  {
    seconds = '0' + String(seconds);
  }
  timeStamp.innerHTML = `
    ${minutes}:${seconds}
  `;
}

// set video time to progress
function setVideoProgress() {
  video.currentTime = ((+progress.value) * video.duration) / 100;
}

// stop video
function stopVideo() {
  video.currentTime = 0;
  video.pause();
}

