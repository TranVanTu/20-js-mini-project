const wordEl = document.querySelector('.word');
const wrongLetterEl = document.querySelector('.wrong-letter');
const playAgainBtn = document.querySelector('#play-btn');
const popup = document.querySelector('.popup-container');
const notification = document.querySelector('.notification-container');
const finalMessage = document.querySelector('#final-message');
const figureParts = document.querySelectorAll('.figure-part');

const words = ['application', 'programming', 'interface', 'wizard'];
let seletedWord = words[Math.floor(Math.random() * words.length)];
console.log(seletedWord);
const correctLetters = [];
const wrongLetters = [];
// show hidden word
function displayWord() {
  wordEl.innerHTML = `
    ${seletedWord.split('').map(letter => `
      <div class="letter">${correctLetters.includes(letter) ? letter : ''}</div>
    `).join('')
    }
  `;
  const innerWord = wordEl.innerText.replace(/\n/g, '');
  if (innerWord === seletedWord)
  {
    finalMessage.innerText = 'Congratulations! You won 😊';
    popup.style.display = 'flex';
  }
}

// keydown letter press
window.addEventListener('keydown', e => {
  // console.log(e.keyCode)
  if (e.keyCode >= 65 && e.keyCode <= 90)
  {
    const letter = String.fromCharCode(e.keyCode).toLowerCase();
    console.log(letter)
    if (seletedWord.includes(letter))
    {
      if (!correctLetters.includes(letter))
      {
        correctLetters.push(letter);
        displayWord();
      } else
      {
        showNotification();
      }
    } else
    {
      if (!wrongLetters.includes(letter))
      {
        wrongLetters.push(letter);
        console.log(wrongLetters);
        updateWrongLetterEl();
      }
    }
  }
});
// show notification
function showNotification() {
  notification.classList.add('show');
  setTimeout(() => {
    notification.classList.remove('show');
  }, 3000);


}
// update wrong letters
function updateWrongLetterEl() {
  // Display wrong letters
  wrongLetterEl.innerHTML = `
    ${wrongLetters.length > 0 ? '<p>Wrong</p>' : ''}
    ${wrongLetters.map(letter =>
    `
        <span>${letter}</span>
      `
  )}
  `;
  // display parts
  figureParts.forEach((part, index) => {
    const errors = wrongLetters.length;
    if (index < errors)
    {
      part.style.display = 'block';
    } else
    {
      part.style.display = 'none';
    }
  });
  // check if lost
  if (wrongLetters.length === figureParts.length)
  {
    finalMessage.innerText = 'Unfortunately you lost';
    popup.style.display = 'flex';
  }
}
// play again btn
playAgainBtn.addEventListener('click', playAgain);
function playAgain() {
  // empty array
  correctLetters.splice(0);
  wrongLetters.splice(0);
  seletedWord = words[Math.floor(Math.random() * words.length)];
  displayWord();
  updateWrongLetterEl();
  popup.style.display = 'none';
}
displayWord();