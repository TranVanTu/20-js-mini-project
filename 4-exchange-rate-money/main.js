var curSelectedOne = document.getElementById('currency-one');
var curAmountOne = document.getElementById('amount-1');
var curSelectedTwo = document.getElementById('currency-two');
var curAmountTwo = document.getElementById('amount-2');
var rateEl = document.getElementById('rate');
var swapBtn = document.getElementById('swap');
// fetch exchange rate and update the DOM
function calculate() {
  var currencyOne = curSelectedOne.value;
  var currencyTwo = curSelectedTwo.value;
  fetch(`https://api.exchangeratesapi.io/latest?base=${currencyOne}`)
    .then(response => response.json())
    .then(data => {
      // console.log(data)
      var rate = data.rates[currencyTwo];
      console.log(rate);
      rateEl.innerHTML = `1 ${currencyOne} = ${rate} ${currencyTwo}`;
      curAmountTwo.value = +(curAmountOne.value * rate).toFixed(2);
    })
    ;
}
// events listener
curSelectedOne.addEventListener('change', calculate);
curAmountOne.addEventListener('input', calculate);
curSelectedTwo.addEventListener('change', calculate);
curAmountTwo.addEventListener('input', calculate);
swapBtn.addEventListener('click', function () {
  var tempSelectedValue = curSelectedOne.value;
  curSelectedOne.value = curSelectedTwo.value;
  curSelectedTwo.value = tempSelectedValue;
  var currencyOne = curSelectedOne.value;
  var currencyTwo = curSelectedTwo.value;
  fetch(`https://api.exchangeratesapi.io/latest?base=${currencyOne}`)
    .then(response => response.json())
    .then(data => {
      // console.log(data)
      var rate = data.rates[currencyTwo];
      console.log(rate);
      rateEl.innerHTML = `1 ${currencyTwo} = ${1 / rate} ${currencyOne}`;
      curAmountTwo.value = +(curAmountOne.value * rate).toFixed(2);
    })
    ;
})
calculate();