const word = document.getElementById('word'),
  text = document.getElementById('text'),
  scoreEl = document.getElementById('score'),
  timeEl = document.getElementById('time'),
  endGameEl = document.getElementById('end-game-container'),
  settingBtn = document.getElementById('setting-btn'),
  settingBoard = document.getElementById('settings'),
  settingForm = document.getElementById('setting-form'),
  difficultySelect = document.getElementById('difficulty');



const words = ['array', 'javascript', 'application', 'game', 'nightmare', 'justify', 'darken', 'cold', 'programming', 'machine', 'intelligent',
  'react', 'angular', 'github', 'amazon', 'alibaba', 'spirit', 'army'];

// init word

let randWord;

// init score

let score = 0;

// init time

let time = 10;

// set difficulty to value in local storage or medium
let difficulty = localStorage.getItem('difficulty') !== null ? localStorage.getItem('difficulty') : 'medium';

// set difficulty selected value
difficultySelect.value = localStorage.getItem('difficulty') !== null ? localStorage.getItem('difficulty') : 'medium';

// focus on text input when start game
text.focus();

// update score

function updateScore() {
  score++;
  scoreEl.innerHTML = score;
}

// start counting down
const timeInterval = setInterval(updateTime, 1000);

function updateTime() {
  time--;
  timeEl.innerHTML = time + ' s';
  if (time < 0)
  {
    clearInterval(timeInterval);
    // show gameover screen
    endGameEl.style.display = 'flex';
    endGameEl.innerHTML = `
      <h1>Time ran out</h1>
       <p>Your final score is ${score}</p>
       <button onclick="location.reload()">Continue Game</button>
    `;
  }
}

// generate random word from array
function getRandomWord() {
  return words[Math.floor(Math.random() * words.length)];
}


// add word to DOM

function addWordToDom() {
  randWord = getRandomWord();
  word.innerHTML = randWord;

}

// continue game

function continueGame() {
  endGameEl.style.display = 'none';
}

addWordToDom();

// event listener


// typing listener
text.addEventListener('input', e => {
  const insertedText = e.target.value;
  // console.log(insertedText)
  if (insertedText === randWord)
  {
    addWordToDom();
    e.target.value = '';
    updateScore();
    if (difficulty === 'hard')
    {
      time += 2;
    } else if (difficulty === 'meidum')
    {
      time += 3;
    } else
    {
      time += 3;
    }
  }
});
// settings listener

settingBtn.addEventListener('click', e => {
  settingBoard.classList.toggle('hide');
});

// settings select

settingForm.addEventListener('change', e => {
  difficulty = e.target.value;
  console.log(difficulty);
  localStorage.setItem('difficulty', difficulty);
  location.reload();
});

