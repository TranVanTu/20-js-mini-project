const postsContainer = document.getElementById('post-container');
const loading = document.getElementById('loader');
const filter = document.getElementById('filter');

let limit = 5;
let page = 1;
async function getPosts() {
  const res = await fetch(`
    https://jsonplaceholder.typicode.com/posts?_limit=${limit}&_page=${page}
  `);
  const data = res.json();
  return data;
}

async function showPosts() {
  // fetch post from API
  const posts = await getPosts();
  posts.forEach(post => {
    const postEl = document.createElement('div');
    postEl.classList.add('post');
    postEl.innerHTML = `
      <div class="number"> ${post.id}</div> 
      <div class="post-info">
        <h2 class="post-title">
          ${post.title}
        </h2>
        <p class="post-body">
          ${post.body}
        </p>
      </div>
    `;
    postsContainer.append(postEl);
  });
}

// show loader and fetch more posts

function showLoading() {
  loading.classList.add('show');
  setTimeout(() => {
    loading.classList.remove('show');
  }, 1000);
  setTimeout(() => {
    page++;
    showPosts();
  }, 100);
}
showPosts();

window.addEventListener('scroll', (e) => {
  const { scrollTop, scrollHeight, clientHeight } = document.documentElement;
  if (scrollTop + clientHeight >= scrollHeight - 5)
  {
    showLoading();
  }
});

// filter posts

filter.addEventListener('keypress', filterPost);

// filter post function

function filterPost(e) {
  const term = e.target.value.toLowerCase();
  posts = document.querySelectorAll('.post');
  posts.forEach(post => {
    const title = post.querySelector('.post-title').innerText;
    const body = post.querySelector('.post-body').innerText;
    if (title.toLowerCase().indexOf(term) > -1 || body.toLowerCase().indexOf(term) > -1)
    {
      post.style.display = 'block';
    } else
    {
      post.style.display = 'none';
    }
  });
}