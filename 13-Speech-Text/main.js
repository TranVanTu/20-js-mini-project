const main = document.getElementById('main'),
  toggleBtn = document.getElementById('toggle-btn'),
  voicesSelect = document.getElementById('voices'),
  closeBtn = document.getElementById('close'),
  textField = document.getElementById('text'),
  readBtn = document.getElementById('read-text'),
  textBox = document.getElementById('text-box');

const data = [
  {
    img: 'https://source.unsplash.com/weekly?dogs',
    text: 'I am hungry. I will get something to eat right now'
  },
  {
    img: 'https://source.unsplash.com/weekly?moon',
    text: 'The moon is dark right now! It so beautiful'
  },
  {
    img: 'https://source.unsplash.com/weekly?cats',
    text: 'The cats is so cute! I will buy it now'
  },
  {
    img: 'https://source.unsplash.com/weekly?traffic',
    text: 'Ha noi traffic is terrible. We need to take actions'
  },
  {
    img: 'https://source.unsplash.com/weekly?money',
    text: 'Money is very important with my life'
  },
  {
    img: 'https://source.unsplash.com/weekly?family',
    text: 'After all, Family is the best'
  },
  {
    img: 'https://source.unsplash.com/weekly?book',
    text: 'Book give you everything you need'
  },
  {
    img: 'https://source.unsplash.com/weekly?technology',
    text: 'The fourth technology industrial were coming soon'
  },
  {
    img: 'https://source.unsplash.com/weekly?football',
    text: 'I like football to much. I used to be a good team player'
  },
];

data.forEach(createBox);


// create boxes function
function createBox(item) {
  const box = document.createElement('div');
  const { img, text } = item;
  box.classList.add('box');
  box.innerHTML = `
      <img src="${img}" alt="${text}" />
      <h3 class="text">
      ${text}
      </h3>
  `;
  box.addEventListener('click', () => {
    setTextMessage(text);
    speakText();

    // add an active effect
    box.classList.add('active');
    setTimeout(() => {
      box.classList.remove('active');
    }, 1000);
  });
  main.append(box);
}

// init speech synth

const message = new SpeechSynthesisUtterance();


// set text message
function setTextMessage(text) {
  message.text = text;

}

// set speak message
function speakText() {
  speechSynthesis.speak(message)
}
// set voice when select change

function setVoice(e) {
  message.voice = voices.find(voice => voice.name === e.target.value);
}

// create an array to store voice
let voices = [];
function getVoices() {
  voices = speechSynthesis.getVoices();
  voices.forEach(voice => {
    const option = document.createElement('option');
    option.innerText = `${voice.name} - ${voice.lang}`;
    option.value = voice.name;
    voicesSelect.append(option);
  })
}

getVoices();
// event listeners

toggleBtn.addEventListener('click', e => {
  textBox.classList.add('show');
});

closeBtn.addEventListener('click', e => {
  textBox.classList.remove('show');
})

// voices changed

speechSynthesis.addEventListener('voiceschanged', getVoices);

// voices select

voicesSelect.addEventListener('change', setVoice);

// read text btn event

readBtn.addEventListener('click', () => {
  setTextMessage(textField.value);
  speakText();
});

