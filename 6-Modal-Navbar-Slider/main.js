const toggleBtn = document.querySelector('.toggle');
const closeBtn = document.querySelector('#close');
const openBtn = document.querySelector('.open');
const modal = document.querySelector('.modal-container');
const navBar = document.querySelector('.navbar');
// toggle nav
toggleBtn.addEventListener('click', () => {
  document.body.classList.add('show-nav');
});
document.body.addEventListener('click', (e) => {
  if (e.target !== navBar && e.target !== toggleBtn)
  {
    document.body.classList.remove('show-nav');
  }
});

// show modal
openBtn.addEventListener('click', () => {
  modal.classList.add('show-modal');
});
// hide modal
closeBtn.addEventListener('click', () => {
  modal.classList.remove('show-modal');
});

// hide modal on outside click
window.addEventListener('click', (e) => {
  e.target == modal ? modal.classList.remove('show-modal') : false;
});