const search = document.getElementById('search'),
  submit = document.getElementById('submit'),
  random = document.getElementById('random'),
  mealsEl = document.getElementById('meals'),
  resultHeading = document.getElementById('result-heading'),
  singleMealEl = document.getElementById('single-meal');

// add submit event
submit.addEventListener('submit', searchMeal);

// search meal and fetch from API
function searchMeal(event) {
  event.preventDefault();

  //  clear single meal
  singleMealEl.innerHTML = '';

  // get search term
  const term = search.value;
  // check for empty
  if (term.trim())
  {
    fetch(`https://www.themealdb.com/api/json/v1/1/search.php?s=${term}`)
      .then(res => res.json())
      .then(
        data => {
          console.log(data);
          resultHeading.innerHTML = `
            <h2>
              Search results for '${term}' is : 
            </h2>
          `;
          if (data.meals == null)
          {
            resultHeading.innerHTML = `
             <p>  There are no search results. Search again </p>
            `;
          } else
          {
            mealsEl.innerHTML = data.meals.map(meal =>
              `
                <div class="meal">
                  <img src="${meal.strMealThumb}" alt="${meal.strMeal}" />
                  <div class="meal-info" data-mealID="${meal.idMeal}" >
                    <h5>
                      ${meal.strMeal}
                    </h5>
                  </div>
                </div>
              `
            ).join('');
            console.log(mealsEl);
          }

        }
      ).catch(error => {
        console.log(error);
      });
    // clear search text
    search.value = '';
  } else
  {
    alert('Please fill out the box');
  }
}

mealsEl.addEventListener('click', e => {
  const mealInfo = e.path.find(item => {
    if (item.classList)
    {
      return item.classList.contains('meal-info');
    } else
    {
      return false;
    }
  });
  if (mealInfo)
  {
    const mealID = mealInfo.getAttribute('data-mealid');
    getMealByID(mealID);
  }
});

// get the meal detail by ID

function getMealByID(mealID) {
  fetch(
    `
      https://www.themealdb.com/api/json/v1/1/lookup.php?i=${mealID}

    `
  ).then(res => res.json())
    .then(data => {
      const meal = data.meals[0];
      addMealToDom(meal);
    })
}

// add meal to the UI

function addMealToDom(meal) {
  const ingredients = [];
  for (let i = 1; i <= 20; i++)
  {
    if (meal[`strIngredient${i}`])
    {
      ingredients.push(` ${meal[`strIngredient${i}`]} - ${meal[`strMeasure${i}`]} `);
    } else
    {
      break;
    }
  }
  console.log(ingredients);
  singleMealEl.innerHTML = `
    <div class="single-meal">
      <h3>
        ${meal.strMeal}
      </h3>
      <img src="${meal.strMealThumb}" alt="${meal.strMeal}" />
      <div class="single-meal-info">
        ${meal.strCategory ? `<p>${meal.strCategory}</p>` : ''}
        ${meal.strArea ? ` <p> ${meal.strArea} </p> ` : ''} 
      </div>
      <div class="main">
        <p>
          ${meal.strInstructions}
        </p>
        <h2>
          Ingredients
        </h2>
        <ul>
          ${ingredients.map(ing => `<li>${ing}</li>`).join('')}
        </ul>
      </div>
    </div>
  `;
}